

Data sent to thingspeak

Usage:

 ./senddata.py 11 22

 Setup:

 git clone https://github.com/adafruit/Adafruit_Python_DHT.git
 sudo apt-get install build-essential python-dev
 sudo python setup.py install

Cron it:

* * * * * /home/pi/Adafruit_Python_DHT/examples/senddata.sh 11 22